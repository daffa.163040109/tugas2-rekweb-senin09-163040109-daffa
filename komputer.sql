-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2018 at 04:53 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komputer`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `stok` int(3) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `name`, `price`, `image`, `stok`, `description`) VALUES
('5c0b9db825ebb', 'Lenovo ThinkPad X1 Carbon (6th Gen)', 17000000, '5c0b9db825ebb.png', 2, 'Processor	\r\nUp to 8th Gen Intel® Core™ vPro™ \r\nUp to 8th Gen Intel® Core™ \r\nOperating System	\r\nWindows 10 Pro 64-bit - Lenovo recommends Windows 10 Pro.\r\nWindows 10 Home\r\nGraphics	\r\nIntegrated Intel® UHD Graphics 620\r\nMemory	\r\n16 GB LPDDR3 2133 MHz\r\n8 GB LPDDR3 2133 MHz\r\nStorage	\r\n1TB SSD OPAL2 PCIe TLC\r\n512 GB SSD OPAL2 PCIe TLC\r\n256 GB SSD OPAL2 PCIe TLC\r\n128 GB SSD SATA\r\nCamera	\r\nHD 720p with ThinkShutter\r\nInfrared (IR) camera (optional; required for use with Windows Hello & doesn’t include ThinkShutter)\r\nBattery	\r\nUp to 15 hours**, Li-Polymer battery integrated 57 Whr with RapidCharge technology\r\n* Based on testing three different product configurations with MobileMark 2014. Battery life varies significantly with settings, usage, and other factors.\r\nDisplay	\r\n14\" HDR WQHD glossy with Dolby Vision™ (2560 x 1440)IPS 500 nits\r\n14\" WQHD (2560 x 1440)IPS anti-glare, 300 nits\r\n14\" FHD IPS Touch (1920 x 1080)anti-glare, 300 nits\r\n14\" FHD IPS (1920 x 1080) anti-glare, 300 nits\r\nAudio	\r\nDolby Atmos® with headphones\r\n360° Noise-cancelling Dual Array Far Field Microphones\r\nSecurity 	\r\nIntegrated Fast Identity Online  (FIDO) Authentication\r\nTouch Fingerprint Reader with Match on Chip technology and Anti-Spoof protection\r\nWindows Hello with facial recognition software (requires IR Camera) or Finger Print Reader\r\nSecure Screen Lock with Glance by Mirametrix (requires IR Camera)\r\ncolours	\r\nBlack with colour coded hinges\r\nI/O (Input/Output) Ports	\r\n2 Intel® Thunderbolt™ 3\r\n2 USB 3.0\r\nHDMI\r\nHeadphone / microphone combo jack\r\n4-in-1 MicroSD card reader (SD, MMC, SDHC, SDXC) \r\nNative Ethernet dongle\r\nNano SIM\r\nConnectivity	\r\nWWAN:optional, Integrated Global Mobile Broadband LTE-A (available in April 2018)\r\nWLAN: Intel® Dual-Band Wireless-AC 2 x 2, AC + Bluetooth® 4.2 (Bluetooth® 4.2 capabilities limited by Windows OS)\r\nNFC optional\r\nDimensions (W x D x H)	323.5 mm x 217.1 mm x 15.95 mm / 12.73\" x 8.54\" x 0.62\"\r\nWeight	Starting at 1.13 kg \r\nWhat\'s in the box	\r\nThinkPad X1 Carbon (6th Gen)\r\n45W / 65W Type-C Power Adapter\r\nQuick Start Guide\r\n3 Year Onsite Warranty: If an issue cannot be resolved by phone, a visit by a technician will be scheduled at the customer location.'),
('5c0b9e8c18a52', 'Lenovo V720 ', 6000000, '5c0b9e8c18a52.png', 10, 'Processor	\r\nUp to 7th gen Intel® Core™ i7\r\nOS	\r\nWindows 10 Pro\r\nDisplay	14\" FHD IPS antiglare (1920 x 1080)\r\nGraphics	NVIDIA® GeForce® 940MX 2GB\r\nCamera	720p HD \r\nBattery	Up to 10.5 hours\r\nAudio	\r\nJBL speakers with Dolby® Atmos certification \r\nIntegrated dual-array, noise-cancelling microphone\r\nSecurity	\r\nOptional touch fingerprint reader\r\nOptional TPM chip\r\nWindows Hello facial recognition software\r\nI/O (Input / Output) Ports	\r\n2 USB 3.0\r\n1 USB-C with Intel Thunderbolt™ 3\r\nHDMI\r\n4-in-1 card reader \r\nCombo audio / mic \r\nConnectivity	\r\nWiFi 2 x 2 ac  \r\nBluetooth® 4.1\r\nHinges	\r\n180 degrees\r\nDimensions (W x D x H)	\r\n12.67\" x 8.73\" x 0.63\" (321.7 mm x 221.8 mm x 15.9 mm) \r\nWeight	\r\nStarting at 3.1 lbs (1.4 kg)\r\nColors	Shark Grey\r\nPreloaded Software	\r\nLenovo App Explorer\r\nLenovo ID\r\nLenovo PC Manager\r\nMicrosoft Office 2016 (Trial offer)'),
('5c0bb8b631edc', 'Lenovo Yoga 920', 9000000, '5c0bb8b631edc.png', 3, 'Prosesor	\r\nUp to 8th Gen Intel Core i7 U processor\r\nSistem Operasi	\r\nWindows 10 Home\r\nDisplay	\r\n13.9\" UHD (3840 x 2160) IPS touchscreen\r\n13.9\" FHD (1920 x 1080) IPS touchscreen\r\nGrafis	\r\nIntegrated Intel HD Graphics\r\nKamera	\r\nHD 720p\r\nMemori	\r\n8 GB DDR4\r\n12 GB DDR4\r\n16 GB DDR4\r\nPenyimpanan	\r\n256 GB PCIe SSD\r\n512 GB PCIe SSD\r\n1 TB PCIe SSD\r\nBaterai	\r\nUp to 10.8 hours with UHD display*\r\nUp to 15.5 hours with FHD display*\r\nBerdasarkan pengujian dengan MobileMark 2014. Daya tahan baterai sangat bervariasitergantung dari pengaturan, penggunaan, faktor lainnya.\r\nAudio	\r\nDolby Atmos (with headphones)\r\n2 JBL Speakers\r\nKeamanan	\r\nFingerprint reader with Windows Hello\r\nI/O (Input / Output) Ports	\r\n1 USB 3.0 with always-on charging\r\n2 USB C (Thunderbolt, PD, DP, USB 3.0 full-function)\r\nAudio jack\r\nKonektivitas	\r\n802.11 a/c 2x2 WiFi\r\nBluetooth 4.1\r\nDimensi (Lebar x Dalam x Tinggi)	\r\n323 mm x 223.5 mm x 13.95 mm / 12.7\" x 8.8\" x 0.5\"\r\nBerat	\r\nStarting at 1.37 kg / 3.0 lbs\r\nWarna	\r\nPlatinum\r\nBronze\r\nCopper');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `no` int(11) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`no`, `userName`, `password`) VALUES
(1, 'tatang', '123'),
(2, 'daffa', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
