<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        
    </li>
    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'products' ? 'active': '' ?>">
        <a href="<?php echo site_url('shopping/products') ?>"class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-boxes"></i>  
            <span>Products</span>
        </a>
    </li>
    <li class="nav-item">
        
    </li>
    <li class="nav-item">
        
    </li>
</ul>
