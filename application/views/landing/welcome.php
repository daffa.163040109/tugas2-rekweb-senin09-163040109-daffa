    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area">

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(<?php echo base_url('css/bg.jpg') ?>);"></div>
                <!-- Slide Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-slides-content text-center">
                                <h6 data-animation="fadeInUp" data-delay="100ms">New Product!</h6>
                                <h2 data-animation="fadeInUp" data-delay="300ms">Lenovo ThinkPad X1 Carbon <span>Lenovo ThinkPad X1 Carbon</span></h2>
                                <a href="<?php echo site_url('page/produk') ?>" data-animation="fadeInUp" data-delay="500ms" href="#" class="btn oneMusic-btn mt-50">Discover <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->
 