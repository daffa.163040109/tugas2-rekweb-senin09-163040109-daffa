<!DOCTYPE html>
<html>
<head>
	<title>Shopping cart dengan codeigniter dan AJAX</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
	<br><br><br><br><br>
<div class="container">
	<div class="row text-center">
		<?php foreach ($data as $row) :?>
		<div class="col-sm-3" href="">
			<div class="card">
				<img class="card-img-top" src="<?php echo base_url().'upload/product/'.$row->image;?>" alt="Card image cap">
				 <a class="film" href="<?php echo base_url('index.php/page/detail/'. $row->product_id) ?>"><b>Lihat Detail</b></a>
				 <h5><?= $row->name ?></h5>
			</div>
		</div>
	<?php endforeach ?>

</div>
</body>
</html>