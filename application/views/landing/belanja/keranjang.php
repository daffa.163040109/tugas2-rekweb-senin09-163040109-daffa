<br><br><br><br><br>

<!-- tabel kerajang -->
<div class="container">  
	<div class="row"></div>
		<h3>Keranjang KomputerKu</h3>
		<?= $this->session->userdata('status_film'); ?>
		<br>
    	
    	<div class="col">
        	<?php echo form_open('Cart/updateCart'); ?>

        	<table class="table" width="80%">

                <tr class="thead-dark">
		      		<th style="text-align: center;">Opsi</th>
                    <th style="text-align: center;">QTY</th>
                    <th>Nama Barang</th>
                    <th style="text-align:center;">Harga</th>
                    <th style="text-align:center;">Total Harga</th>
                </tr>

                <?php $i = 1; ?>

                <?php foreach ($this->cart->contents() as $items): ?>

                <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

                <tr>
        	        <td align="center"><a href="<?= base_url('index.php/Cart/hapusCart/'. $items['rowid']) ?>">Hapus</a></td>
                        <td><?php echo form_input(array('name' => $i.'qty', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
                        <td><?php echo $items['name']; ?>
                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                <p> <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                        <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
                                        <?php endforeach; ?>
                                </p>
                        `       <?php endif; ?>
                        </td>
                        <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
                        <td style="text-align:right">Rp<?php echo $this->cart->format_number($items['subtotal']); ?></td>
                </tr>

                <?php $i++; ?>

                <?php endforeach; ?>

                <tr>
                        <td colspan="3"> </td>
                        <td class="right"><strong>Total</strong></td>
                        <td class="right">Rp<?php echo $this->cart->format_number($this->cart->total()); ?></td>
                </tr>

        </table>
        <p class="text-center">
        	<?php echo form_submit('', 'Update Keranjang',"class='btn btn-outline-dark'"); ?>
        	<a href="<?= base_url('index.php/page/produk') ?>" class="btn btn-outline-info">Lanjut Belanja</a>
        	<a href="<?= base_url('Customer/checkOut') ?>" class="btn btn-outline-warning">Check Out</a>
        </p>
    </div>

     <div class="col">
            
    </div>

</div>
</div> 