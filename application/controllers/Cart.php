<?php

class Cart extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('keranjang_model');
        $this->load->model('cart_model');
        $this->load->model('Product_model');
        $this->load->library('cart');
	}

	
	public function add_to_cart($id){
		$getData = $this->cart_model->getElement($id);
		$data = array('id'=>$getData->product_id, 
					'name'=>$getData->name,
					'qty' => 1, 
					'price'=>$getData->price);
		$i = 0;
		foreach ($this->cart->contents() as $key) {

			if ($getData->stok <= $key['qty']) {
				$this->session->set_flashdata('stok', '<h3 style="color: red;">Stok Film Kurang</h3>');
				redirect('Page/keranjang');
			}
		}
		$this->cart->insert($data);
		$halaman['total_cart'] = $i;
		$halaman['content'] = 'keranjang';
		$this->load->view("templates/header");
		$this->load->view("landing/belanja/keranjang");
		$this->load->view("templates/footer");
	}

	// Tampil Cart
	public function cart(){
		$data["content"] = "keranjang";
		$this->load->view("templates/header");
		$this->load->view("landing/belanja/keranjang");
		$this->load->view("templates/footer");
	}

	// Hapus Cart
	public function hapusCart($rowid){
		$this->cart->update(array('rowid' => $rowid, 'qty'=>0));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'Keranjang';
		$this->load->view("templates/header");
		$this->load->view("landing/belanja/keranjang");
		$this->load->view("templates/footer");
	}

	// Update Cart
	public function updateCart(){
		$i = 1;
		$u = 0;
		foreach ($this->cart->contents() as $fl) {
			$getData = $this->Cart_model->getElement($fl["id"]);
			if ($getData->stok < $_POST[$i.'qty']) {
				$this->session->set_flashdata('status_film', '<h3 style="color: red;">Stok Film Kurang</h3>');
				redirect('Cart/cart');
			}
			else{
				$this->cart->update(array('rowid'=>$fl['rowid'], 'stok'=>$_POST[$i.'stok']));
				$i++;
				$u++;
			}
		}
		$data["total_cart"] = $u;
		$data['content'] = 'Keranjang';
		$this->load->view("templates/header");
		$this->load->view("landing/belanja/keranjang");
		$this->load->view("templates/footer");
	}

}