<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Page extends CI_Controller {
    public function __construct()
    {   
        parent::__construct();
        $this->load->library('cart');
        $this->load->model('keranjang_model');
        $this->load->model('cart_model');
        $this->load->model('Product_model');
    }
    public function index()
        {
            $data['produk'] = $this->keranjang_model->get_produk_all();
            $this->load->view('templates/header', $data);
            $this->load->view('landing/welcome',$data);
            $this->load->view('templates/footer');
           
        }
    public function about()
        {
            $data['produk'] = $this->keranjang_model->get_produk_all();
            $this->load->view('templates/header', $data);
            $this->load->view('landing/pages/about');
            $this->load->view('templates/footer');
        }
    public function produk()
        {
            
            $data['produk'] = $this->keranjang_model->get_produk_all();
            $data['data'] = $this->cart_model->get_all_produk();
            $this->load->view('templates/header', $data);
            $this->load->view('landing/belanja/laptop',$data);
            $this->load->view('templates/footer');
        }
        public function detail($id){

        $data['detail'] = $this->cart_model->getElement($id);
        $data['produk'] = $this->keranjang_model->get_produk_all();
        $data['data'] = $this->cart_model->get_all_produk();
        $this->load->view('templates/header', $data);
        $this->load->view('landing/belanja/detail', $data);
        $this->load->view('templates/footer');
    }
    public function keranjang(){
        $data['produk'] = $this->keranjang_model->get_produk_all();
        $this->load->view('templates/header', $data);
        $this->load->view('landing/belanja/keranjang', $data);
        $this->load->view('templates/footer');
    }
    public function admin(){
        $data['produk'] = $this->keranjang_model->get_produk_all();
        $this->load->view('admin/overview', $data);
    }



}